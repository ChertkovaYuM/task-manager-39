package ru.tsc.chertkova.tm.exception.user;

import ru.tsc.chertkova.tm.exception.field.AbstractFieldException;

public final class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
