package ru.tsc.chertkova.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.Domain;
import ru.tsc.chertkova.tm.dto.request.data.DataJsonFasterXmlLoadRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DomainJsonFasterXmlLoadCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load date from json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getServiceLocator().getDomainEndpoint().loadDataJsonFasterXml(new DataJsonFasterXmlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
