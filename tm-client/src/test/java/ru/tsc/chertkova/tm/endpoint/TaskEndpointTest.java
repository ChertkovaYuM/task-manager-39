package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.dto.request.task.*;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;
import ru.tsc.chertkova.tm.dto.response.task.TaskChangeStatusByIdResponse;
import ru.tsc.chertkova.tm.dto.response.task.TaskCreateResponse;
import ru.tsc.chertkova.tm.dto.response.task.TaskShowByIdResponse;
import ru.tsc.chertkova.tm.dto.response.task.TaskUpdateByIdResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserLoginResponse;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.marker.IntegrationCategory;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getHost();

    @NotNull
    private final String port = propertyService.getPort();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Nullable
    private Task taskBefore;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(new TaskClearRequest(token)));
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(
                new TaskCreateRequest(token, "test", "test", "1")
        );
        taskBefore = createResponse.getTask();
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(null, taskBefore.getId(), Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, taskBefore.getId(), null)));
        TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, taskBefore.getId(), Status.IN_PROGRESS));
        Assert.assertNotNull(response);
        @Nullable Task Task = response.getTask();
        Assert.assertNotEquals(taskBefore.getStatus(), Task.getStatus());
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest(null, "", "", "1")));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest(token, "", "", "1")));
        @NotNull final String TaskName = "name";
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "tasktask", "description", "1"));
        Assert.assertNotNull(response);
        @Nullable Task Task = response.getTask();
        Assert.assertEquals(TaskName, Task.getName());
    }

    @Test
    public void removeTaskByIdTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(null, taskBefore.getId())));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, taskBefore.getId())));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks());
    }

    @Test
    public void showTaskById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(new TaskShowByIdRequest(null, taskBefore.getId())));
        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, taskBefore.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(null, taskBefore.getId(), "", "")));
        @NotNull final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, taskBefore.getId(), "task1", "vot kak-to tak"));
        Assert.assertNotNull(response);
        @Nullable Task Task = response.getTask();
        Assert.assertNotNull(Task);
        Assert.assertNotEquals(taskBefore.getName(), Task.getName());
    }

}
