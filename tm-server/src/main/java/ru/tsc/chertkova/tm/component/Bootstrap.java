package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.endpoint.*;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.endpoint.*;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.service.*;
import ru.tsc.chertkova.tm.util.DateUtil;
import ru.tsc.chertkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, userService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService, userService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);


    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        final String host = "localhost";
        final String port = "8080";
        final String name = endpoint.getClass().getSimpleName();
        final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initPID();
        initData();
        loggerService.info("** TASK MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        //backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER STOPPED **");
        //backup.stop();
    }

    @SneakyThrows
    public void initData() {
        try {
            User user = userService.add(new User("test", "test",
                    "test@test.ru", "test", "test", "test"));
            Project project = new Project("project inache", Status.NOT_STARTED, DateUtil.toDate("04.10.2020"));
            project.setUserId(user.getId());
            projectService.add(project);
            Task task1 = new Task("task kak-to", Status.NOT_STARTED, DateUtil.toDate("04.12.2019"));
            task1.setProjectId(project.getId());
            task1.setUserId(user.getId());
            taskService.add(task1);
            Task task2 = new Task("task tak", Status.COMPLETED, DateUtil.toDate("04.9.2019"));
            task2.setProjectId(project.getId());
            task2.setUserId(user.getId());
            taskService.add(task2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            User user = new User("admin", "admin", "admin@admin.admin",
                    "admin", "admin", "admin");
            user.setRole(Role.ADMIN);
            userService.add(user);
            Project project = new Project("project nikak", Status.IN_PROGRESS, DateUtil.toDate("04.10.2021"));
            project.setUserId(user.getId());
            projectService.add(project);
            Task task1 = new Task("task vot", Status.IN_PROGRESS, DateUtil.toDate("04.10.2019"));
            task1.setProjectId(project.getId());
            task1.setUserId(user.getId());
            taskService.add(task1);
            Project project1 = new Project("project no", Status.COMPLETED, DateUtil.toDate("04.10.2018"));
            project1.setUserId(user.getId());
            projectService.add(project1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
