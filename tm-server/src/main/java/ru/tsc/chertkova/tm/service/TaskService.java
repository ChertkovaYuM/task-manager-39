package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class TaskService extends AbstractUserOwnerService<Task> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService, @Nullable IUserService userService) {
        super(connectionService, userService);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable List<Task> tasks;
        try {
            tasks = taskRepository.findAllByProjectId(userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return tasks;
    }

    @Override
    @Nullable
    public Task add(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUserId()).orElseThrow(UserNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String id = task.getId();
        @NotNull final String projectId = task.getProjectId();
        @NotNull final String name = task.getName();
        @Nullable final String description = task.getDescription();
        @NotNull final String status = task.getStatus().toString();
        @NotNull final Timestamp created = new Timestamp(task.getCreated().getTime());
        @Nullable final Timestamp started = task.getDateBegin() == null ?
                null : new Timestamp(task.getDateBegin().getTime());
        @Nullable final Timestamp completed = task.getDateEnd() == null ?
                null : new Timestamp(task.getDateEnd().getTime());
        @NotNull final String userId = task.getUserId();
        try {
            taskRepository.add(id, userId, name, description,
                    status, created, started, completed, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        try {
            taskRepository.update(id, userId, name, description);
            sqlSession.commit();
            task = findById(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable Task task = findById(userId, id);
        try {
            taskRepository.changeStatus(id, userId, status.getDisplayName());
            sqlSession.commit();
            task = findById(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull int count = taskRepository.existsById(id);
        return count > 0;
    }

    @Override
    public @Nullable Task findById(@Nullable final String userId,
                                   @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable Task task;
        try {
            task = taskRepository.findById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

    @Override
    public Task removeById(@Nullable final String userId,
                           @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final Task task = findById(userId, id);
        try {
            taskRepository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

    @Override
    public Task remove(@Nullable final String userId,
                       @Nullable final Task task) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(findById(task.getUserId(), task.getId())).orElseThrow(TaskNotFoundException::new);
        removeById(task.getUserId(), task.getId());
        return task;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        int size = 0;
        try {
            size = taskRepository.getSize(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return size;
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable List<Task> tasks;
        try {
            tasks = taskRepository.findAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId,
                              @Nullable final Comparator<Task> comparator) {
        return findAll(userId);
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId,
                              @Nullable final Sort sort) {
        return findAll(userId);
    }

    @Override
    @Nullable
    public List<Task> addAll(@NotNull final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            for (Task t : tasks) {
                taskRepository.add(t.getId(), t.getUserId(),
                        t.getName(), t.getDescription(),
                        Objects.requireNonNull(t.getStatus().getDisplayName()),
                        new Timestamp(t.getCreated().getTime()),
                        new Timestamp(t.getDateBegin().getTime()),
                        new Timestamp(t.getDateEnd().getTime()), t.getProjectId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> removeAll(@Nullable final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            for (Task t : tasks) {
                taskRepository.removeById(t.getUserId(), t.getId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return tasks;
    }

}
