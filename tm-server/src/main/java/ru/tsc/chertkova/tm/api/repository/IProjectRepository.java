package ru.tsc.chertkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Project;

import java.sql.Timestamp;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO public.\"TM_PROJECT\" " +
            "(\"ID\",\"USER_ID\",\"NAME\",\"DESCRIPTION\",\"STATUS\"," +
            "\"CREATED_DT\"," +
            "\"STARTED_DT\",\"COMPLETED_DT\")" +
            " VALUES (#{id},#{userId},#{name},#{description}," +
            "#{status},#{created},#{dateBegin},#{dateEnd})")
    void add(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId,
             @Param("name") @NotNull String name, @Param("description") @NotNull String description,
             @Param("status") @NotNull String status, @Param("created") @NotNull Timestamp created,
             @Param("dateBegin") @NotNull Timestamp dateBegin, @Param("dateEnd") @NotNull Timestamp dateEnd);

    @Delete("DELETE FROM public.\"TM_PROJECT\" WHERE \"USER_ID\"=#{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"TM_PROJECT\" WHERE \"USER_ID\"=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    List<Project> findAll(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"TM_PROJECT\" WHERE \"USER_ID\"=#{userId} AND \"ID\"=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateBegin", column = "STARTED_DT"),
            @Result(property = "dateEnd", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    Project findById(@Param("userId") @NotNull String userId,
                     @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM public.\"TM_PROJECT\" WHERE \"USER_ID\"=#{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM public.\"TM_PROJECT\" WHERE \"ID\"=#{id} LIMIT 1")
    void removeById(@Param("userId") @NotNull String userId,
                    @Param("id") @NotNull String id);

    @Update("UPDATE public.\"TM_PROJECT\" " +
            "SET \"NAME\"=#{name} AND \"DESCRIPTION\"=#{description} " +
            "WHERE \"ID\" = #{id} AND \"USER_ID\"=#{userId}")
    void update(@Param("id") @NotNull String id,
                @Param("userId") @NotNull String userId,
                @Param("name") @NotNull String name,
                @Param("description") @NotNull String description);

    @Update("UPDATE public.\"TM_PROJECT\" " +
            "SET \"STATUS\"=#{status} " +
            "WHERE \"ID\" = #{id} AND \"USER_ID\"=#{userId}")
    void changeStatus(@Param("id") @NotNull String id,
                      @Param("userId") @NotNull String userId,
                      @Param("status") @NotNull String status);

    @Select("SELECT COUNT(1) FROM public.\"TM_PROJECT\" WHERE \"ID\"=#{id}")
    int existsById(@Param("id") @Nullable String id);

}
