package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId);

    @Nullable
    Task add(@Nullable Task task);

    @Nullable
    Task updateById(
            @Nullable String id,
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description);

    @Nullable
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status);

    boolean existsById(@Nullable String id);

    @Nullable
    Task findById(@Nullable String userId,
                  @Nullable String id);

    Task removeById(@Nullable String userId,
                    @Nullable String id);

    Task remove(@Nullable String userId,
                @Nullable Task task);

    int getSize(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    List<Task> findAll(@Nullable String userId);

    @Nullable
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Comparator<Task> comparator);

    @Nullable
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Sort sort);

    @Nullable
    List<Task> addAll(@NotNull List<Task> tasks);

    @Nullable
    List<Task> removeAll(@Nullable List<Task> tasks);

}
