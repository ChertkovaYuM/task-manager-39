package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginExistsException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.util.HashUtil;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable User user;
        try {
            user = userRepository.findByLogin(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable User user;
        try {
            user = userRepository.findByEmail(email);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User remove(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Optional.ofNullable(userRepository.findById(user.getId())).orElseThrow(UserNotFoundException::new);
        try {
            userRepository.removeById(user.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        int count = 0;
        try {
            count = userRepository.existsById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        try {
            userRepository.removeByLogin(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExists(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        int count = 0;
        try {
            count = userRepository.isLoginExist(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return count > 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmailExists(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        int count = 0;
        try {
            count = userRepository.isEmailExist(email);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId,
                            @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Optional.ofNullable(userRepository.findById(userId)).orElseThrow(UserNotFoundException::new);
        @Nullable User user;
        try {
            userRepository.setPassword(userId, password);
            sqlSession.commit();
            user = userRepository.findById(userId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUser(@Nullable final String userId,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Optional.ofNullable(userRepository.findById(userId)).orElseThrow(UserNotFoundException::new);
        @Nullable User user;
        try {
            userRepository.update(userId, firstName, middleName, lastName);
            sqlSession.commit();
            user = userRepository.findById(userId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        @Nullable User user;
        try {
            userRepository.setLockedFlag(login, true);
            sqlSession.commit();
            user = userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        @Nullable User user;
        try {
            userRepository.setLockedFlag(login, false);
            sqlSession.commit();
            user = userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User add(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
        Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            if (userRepository.isLoginExist(user.getLogin()) > 0) throw new LoginExistsException();
            if (userRepository.isEmailExist(user.getEmail()) > 0) throw new EmailExistsException();
            userRepository.add(user.getId(), user.getLogin(),
                    user.getFirstName(), user.getMiddleName(),
                    user.getLastName(), new Timestamp(user.getCreated().getTime()),
                    user.getEmail(), HashUtil.md5(user.getPasswordHash()),
                    user.getRole().getDisplayName(),
                    user.getLocked());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User updateById(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String middleName,
                           @Nullable final String lastName) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        @Nullable User user;
        try {
            userRepository.update(id, firstName, middleName, lastName);
            sqlSession.commit();
            user = userRepository.findById(id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable User user;
        try {
            user = userRepository.findById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable User user = userRepository.findById(id);
        try {
            userRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return user;
    }

    @Override
    public int getSize(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable int count = 0;
        try {
            count = userRepository.getSize();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return count;
    }

    @Override
    public void clear(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @Override
    @Nullable
    public List<User> findAll(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable List<User> users;
        try {
            users = userRepository.findAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return users;
    }

    @Override
    @Nullable
    public List<User> addAll(@Nullable List<User> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            for (User user : users) {
                Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
                Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
                Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
                userRepository.add(user.getId(), user.getLogin(),
                        user.getFirstName(), user.getMiddleName(),
                        user.getLastName(), new Timestamp(user.getCreated().getTime()),
                        user.getEmail(), user.getPasswordHash(),
                        user.getRole().getDisplayName(),
                        user.getLocked());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return users;
    }

    @Override
    @Nullable
    public List<User> removeAll(@Nullable final List<User> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            for (User u : users) {
                userRepository.removeById(u.getId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return users;
    }

}
