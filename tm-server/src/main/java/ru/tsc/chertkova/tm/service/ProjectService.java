package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.model.Project;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnerService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService, @Nullable IUserService userService) {
        super(connectionService, userService);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(project.getUserId()).orElseThrow(UserNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String id = project.getId();
        @NotNull final String name = project.getName();
        @Nullable final String description = project.getDescription();
        @NotNull final String status = project.getStatus().toString();
        @NotNull final Timestamp created = new Timestamp(project.getCreated().getTime());
        @Nullable final Timestamp started = project.getDateBegin() == null ?
                null : new Timestamp(project.getDateBegin().getTime());
        @Nullable final Timestamp completed = project.getDateEnd() == null ?
                null : new Timestamp(project.getDateEnd().getTime());
        @NotNull final String userId = project.getUserId();
        try {
            projectRepository.add(id, userId, name, description, status, created, started, completed);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String id,
                              @Nullable final String userId,
                              @Nullable final String name,
                              @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable Project project = findById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        try {
            projectRepository.update(id, userId, name, description);
            sqlSession.commit();
            project = findById(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(@Nullable final String userId,
                                           @Nullable final String id,
                                           @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable Project project;
        try {
            projectRepository.changeStatus(id, userId, status.getDisplayName());
            sqlSession.commit();
            project = findById(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull int count = projectRepository.existsById(id);
        return count > 0;
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId,
                            @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable Project project;
        try {
            project = projectRepository.findById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return project;
    }

    @Override
    @SneakyThrows
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final Project project = findById(userId, id);
        try {
            projectRepository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return project;
    }

    @Override
    @SneakyThrows
    public Project remove(@Nullable final String userId,
                          @Nullable final Project project) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(findById(project.getUserId(), project.getId())).orElseThrow(ProjectNotFoundException::new);
        removeById(project.getUserId(), project.getId());
        return project;
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        int size = 0;
        try {
            size = projectRepository.getSize(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return size;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable List<Project> projects;
        try {
            projects = projectRepository.findAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId,
                                 @Nullable final Comparator<Project> comparator) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable List<Project> projects;
        try {
            projects = projectRepository.findAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId,
                                 @Nullable final Sort sort) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable List<Project> projects;
        try {
            projects = projectRepository.findAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return projects;
    }

    @Nullable
    @Override
    public List<Project> addAll(@NotNull final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (Project p : projects) {
                projectRepository.add(p.getId(), p.getUserId(),
                        p.getName(), p.getDescription(),
                        Objects.requireNonNull(p.getStatus().getDisplayName()),
                        new Timestamp(p.getCreated().getTime()),
                        new Timestamp(p.getDateBegin().getTime()),
                        new Timestamp(p.getDateEnd().getTime()));
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return projects;
    }

    @Nullable
    @Override
    public List<Project> removeAll(@Nullable final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (Project p : projects) {
                projectRepository.removeById(p.getUserId(), p.getId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return projects;
    }

}
