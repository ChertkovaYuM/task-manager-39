package ru.tsc.chertkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.Task;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    @Select("SELECT * FROM public.\"TM_TASK\" WHERE \"USER_ID\"=#{userId} AND \"PROJECT_ID\"=#{projectId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    List<Task> findAllByProjectId(@Param("userId") @Nullable String userId,
                                  @Param("projectId") @Nullable String projectId);

    @Insert("INSERT INTO public.\"TM_TASK\" " +
            "(\"ID\",\"USER_ID\",\"NAME\",\"DESCRIPTION\",\"STATUS\"," +
            "\"CREATED_DT\"," +
            "\"STARTED_DT\",\"COMPLETED_DT\",\"PROJECT_ID\")" +
            " VALUES (#{id},#{userId},#{name},#{description}," +
            "#{status},#{created},#{dateBegin},#{dateEnd},#{projectId})")
    void add(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId,
             @Param("name") @NotNull String name, @Param("description") @NotNull String description,
             @Param("status") @NotNull String status, @Param("created") @NotNull Timestamp created,
             @Param("dateBegin") @NotNull Timestamp dateBegin, @Param("dateEnd") @NotNull Timestamp dateEnd,
             @Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM public.\"TM_TASK\" WHERE \"USER_ID\"=#{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"TM_TASK\" WHERE \"USER_ID\"=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    List<Task> findAll(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"TM_TASK\" WHERE \"USER_ID\"=#{userId} AND \"ID\"=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateBegin", column = "STARTED_DT"),
            @Result(property = "dateEnd", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    Task findById(@Param("userId") @NotNull String userId,
                  @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM public.\"TM_TASK\" WHERE \"USER_ID\"=#{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM public.\"TM_TASK\" WHERE \"ID\"=#{id} LIMIT 1")
    void removeById(@Param("userId") @NotNull String userId,
                    @Param("id") @NotNull String id);

    @Update("UPDATE public.\"TM_TASK\" " +
            "SET \"NAME\"=#{name} AND \"DESCRIPTION\"=#{description} " +
            "WHERE \"ID\" = #{id} AND \"USER_ID\"=#{userId}")
    void update(@Param("id") @NotNull String id,
                @Param("userId") @NotNull String userId,
                @Param("name") @NotNull String name,
                @Param("description") @NotNull String description);

    @Update("UPDATE public.\"TM_TASK\" " +
            "SET \"STATUS\"=#{status} " +
            "WHERE \"ID\" = #{id} AND \"USER_ID\"=#{userId}")
    void changeStatus(@Param("id") @NotNull String id,
                      @Param("userId") @NotNull String userId,
                      @Param("status") @NotNull String status);

    @Select("SELECT COUNT(1) FROM public.\"TM_TASK\" WHERE \"ID\"=#{id}")
    int existsById(@Param("id") @Nullable String id);

}
