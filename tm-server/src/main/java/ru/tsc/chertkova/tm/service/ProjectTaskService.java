package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IProjectTaskService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public class ProjectTaskService extends AbstractUserOwnerService<Project> implements IProjectTaskService {

    public ProjectTaskService(@NotNull IConnectionService connectionService, @Nullable IUserService userService) {
        super(connectionService, userService);
    }

    @Override
    @SneakyThrows
    public Task bindTaskToProject(@Nullable final String userId,
                                  @Nullable final String projectId,
                                  @Nullable final String taskId) {
//        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
//        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
//        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
//        @Nullable Task task;
//        try {
//            task = taskRepository.findById(taskId);
//            if (task == null) throw new TaskNotFoundException();
//            @NotNull final String sqlCommand = String.format(
//                    "UPDATE public.\"TM_TASK\" SET %s = ? WHERE \"ID\" = ?",
//                    "\"PROJECT_ID\""
//            );
//            @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand);
//            preparedStatement.setString(1, projectId);
//            preparedStatement.setString(2, taskId);
//            connection.commit();
//        } catch (@NotNull final Exception e) {
//            connection.rollback();
//            throw e;
//        }
//        return task;
        return null;
    }

    @Override
    @SneakyThrows
    public Task unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
//        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
//        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
//        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
//        @Nullable Task task;
//        try {
//            task = taskRepository.findById(taskId);
//            if (task == null) throw new TaskNotFoundException();
//            @NotNull final String sqlCommand = String.format(
//                    "UPDATE public.\"TM_TASK\" SET %s = ? WHERE \"ID\" = ?",
//                    "\"PROJECT_ID\""
//            );
//            @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand);
//            preparedStatement.setString(1, null);
//            preparedStatement.setString(2, taskId);
//            connection.commit();
//        } catch (@NotNull final Exception e) {
//            connection.rollback();
//            throw e;
//        }
//        return task;
        return null;
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
//        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
//        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
//        for (@Nullable final Task task : tasks) taskRepository.removeById(task.getId());
//        projectRepository.removeById(projectId);
    }

}
