package ru.tsc.chertkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.User;

import java.sql.Timestamp;
import java.util.List;

public interface IUserRepository {

    @Nullable
    @Select("SELECT * FROM public.\"TM_USER\" WHERE \"LOGIN\"=#{login} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "created", column = "CREATED"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "password", column = "PASSWORD"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "locked", column = "LOCKED")
    })
    User findByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT * FROM public.\"TM_USER\" WHERE \"EMAIL\"=#{email} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "created", column = "CREATED"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "password", column = "PASSWORD"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "locked", column = "LOCKED")
    })
    User findByEmail(@Param("email") @NotNull String email);

    @Delete("DELETE FROM public.\"TM_USER\" WHERE \"LOGIN\"=#{login} LIMIT 1")
    void removeByLogin(@Param("login") @NotNull String login);

    @Select("SELECT COUNT(1) FROM public.\"TM_USER\" WHERE \"LOGIN\"=#{login} LIMIT 1")
    int isLoginExist(@Param("login") @NotNull String login);

    @Select("SELECT COUNT(1) FROM public.\"TM_USER\" WHERE \"EMAIL\"=#{email} LIMIT 1")
    int isEmailExist(@Param("email") @NotNull String email);

    @Insert("INSERT INTO public.\"TM_USER\" " +
            "(\"ID\",\"LOGIN\",\"FIRST_NAME\"," +
            "\"MIDDLE_NAME\",\"LAST_NAME\"," +
            "\"CREATED\",\"EMAIL\",\"PASSWORD\"," +
            "\"ROLE\",\"LOCKED\")" +
            " VALUES (#{id},#{login},#{firstName},#{middleName}," +
            "#{lastName},#{created}," +
            "#{email},#{password},#{role},#{locked})")
    void add(@Param("id") @NotNull String id, @Param("login") @NotNull String login,
             @Param("firstName") @Nullable String firstName, @Param("middleName") @Nullable String middleName,
             @Param("lastName") @Nullable String lastName, @Param("created") @NotNull Timestamp created,
             @Param("email") @NotNull String email, @Param("password") @NotNull String password,
             @Param("role") @Nullable String role, @Param("locked") @NotNull Boolean locked);

    @Delete("DELETE FROM public.\"TM_USER\"")
    void clear();

    @Nullable
    @Select("SELECT * FROM public.\"TM_USER\"")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "created", column = "CREATED"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "password", column = "PASSWORD"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "locked", column = "LOCKED")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM public.\"TM_USER\" WHERE \"ID\"=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "created", column = "CREATED"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "password", column = "PASSWORD"),
            @Result(property = "role", column = "ROLE"),
            @Result(property = "locked", column = "LOCKED")
    })
    User findById(@Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM public.\"TM_USER\"")
    int getSize();

    @Delete("DELETE FROM public.\"TM_USER\" WHERE \"ID\"=#{id} LIMIT 1")
    void removeById(@Param("id") @NotNull String id);

    @Update("UPDATE public.\"TM_USER\" " +
            "SET \"FIRST_NAME\"=#{firstName} AND \"MIDDLE_NAME\"=#{middleName} " +
            "AND \"LAST_NAME\"=#{lastName} " +
            "WHERE \"ID\" = #{id}")
    void update(@Param("id") @NotNull String id,
                @Param("firstName") @NotNull String firstName,
                @Param("middleName") @NotNull String middleName,
                @Param("lastName") @NotNull String lastName);

    @Update("UPDATE public.\"TM_USER\" " +
            "SET \"ROLE\"=#{role} " +
            "WHERE \"ID\" = #{id}")
    void changeRole(@Param("id") @NotNull String id,
                    @Param("role") @NotNull String role);

    @Select("SELECT COUNT(1) FROM public.\"TM_USER\" WHERE \"ID\"=#{id}")
    int existsById(@Param("id") @NotNull String id);

    @Update("UPDATE public.\"TM_USER\" " +
            "SET \"PASSWORD\"=#{password} " +
            "WHERE \"ID\" = #{id}")
    void setPassword(@Param("id") @NotNull String id,
                     @Param("password") @NotNull String password);

    @Update("UPDATE public.\"TM_USER\" " +
            "SET \"LOCKED\"=#{locked} " +
            "WHERE \"LOGIN\" = #{login}")
    void setLockedFlag(@Param("login") @NotNull String login,
                       @Param("locked") @NotNull Boolean locked);

}
